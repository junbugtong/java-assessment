Description: The appliation is capable of retrieving json response from a given URL and displaying this response to user.

---------------------
Pages functionality
---------------------
Home page
	- All profiles will be displayed in a table.
	- User can search a profile.
	- User can sort the values in the table.
	
Profile page
	- Selected profile from the homepage will be displayed on this page.

	
---------------------------------
Framework and technologies used
---------------------------------
	- Backend
		- Spring MVC
		- Jackson for parsing the JSON response into POJO
		- Log4j for logging
		
	- Frontend
		- JSP
		- HTML
		- CSS
		- Javascript
		- JQuery
		- Ajax
		- Bootstrap
		
		
Author: Jun Bugtong
Date: May 19, 2018