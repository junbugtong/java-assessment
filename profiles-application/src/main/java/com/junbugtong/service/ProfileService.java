package com.junbugtong.service;

import java.io.IOException;
import java.util.List;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.junbugtong.model.Profile;

/**
 * @author Jun
 *
 */
public interface ProfileService {


  /**
   * Method to get all the profiles from given JSON url
   * 
   * @return List<Profile> list of profiles from given JSON url
   * @throws JsonParseException
   * @throws JsonMappingException
   * @throws IOException
   */
  public List<Profile> getAllProfile() throws JsonParseException, JsonMappingException, IOException;

  /**
   * Method to get all the selected profile from given JSON url based on id
   * 
   * @param id
   * @return Profile returned profile based on the id
   * @throws JsonParseException
   * @throws JsonMappingException
   * @throws IOException
   */
  public Profile getProfile(String id) throws JsonParseException, JsonMappingException, IOException;
}
