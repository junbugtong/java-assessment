package com.junbugtong.service;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.junbugtong.model.Profile;

/**
 * @author Jun
 *
 */
@Service
public class ProfileServiceImpl implements ProfileService {

  private static final Logger LOGGER = Logger.getLogger(ProfileServiceImpl.class);

  private final URL jsonUrl =
      new URL("http://s3-ap-southeast-1.amazonaws.com/fundo/js/profiles.json");

  public ProfileServiceImpl() throws MalformedURLException {}

  /*
   * (non-Javadoc)
   * 
   * @see com.junbugtong.service.ProfileService#getAllProfile()
   */
  @Override
  public List<Profile> getAllProfile()
      throws JsonParseException, JsonMappingException, IOException {
    List<Profile> listProfile = new ArrayList<Profile>();

    ObjectMapper objectMapper = new ObjectMapper();
    try {
      listProfile = objectMapper.readValue(jsonUrl, new TypeReference<List<Profile>>() {});
    } catch (JsonParseException e) {
      LOGGER.error("JSON response could not be parsed.");
      throw e;
    } catch (JsonMappingException e) {
      LOGGER.error("Incorrect JSON response mapping.");
      throw e;
    } catch (IOException e) {
      LOGGER.error("Error occurred while retrieving JSON");
      throw e;
    }

    return listProfile;
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.junbugtong.service.ProfileService#getProfile(java.lang.String)
   */
  @Override
  public Profile getProfile(String id)
      throws JsonParseException, JsonMappingException, IOException {
    ObjectMapper objectMapper = new ObjectMapper();
    List<Profile> listProfile = new ArrayList<Profile>();
    Profile result = null;
    try {
      listProfile = objectMapper.readValue(jsonUrl, new TypeReference<List<Profile>>() {});

      if (!listProfile.isEmpty()) {
        result = listProfile.stream().filter(profile -> profile.getId().equalsIgnoreCase(id))
            .findFirst().get();
      }

    } catch (JsonParseException e) {
      LOGGER.error("JSON response could not be parsed.");
      throw e;
    } catch (JsonMappingException e) {
      LOGGER.error("Incorrect JSON response mapping.");
      throw e;
    } catch (IOException e) {
      LOGGER.error("Error occurred while retrieving JSON");
      throw e;
    }
    return result;
  }

}
