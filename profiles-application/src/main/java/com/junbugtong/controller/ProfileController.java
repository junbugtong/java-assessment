package com.junbugtong.controller;

import java.io.IOException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.junbugtong.model.Profile;
import com.junbugtong.service.ProfileService;

/**
 * @author Jun
 *
 */
@Controller
public class ProfileController {

  @Autowired
  ProfileService profileService;

  /**
   * Handler mapping method to load home page.
   */
  @RequestMapping(path = "/", method = RequestMethod.GET)
  public String goHome() {
    return "home";
  }

  /**
   * Handler mapping method to load profile page.
   * 
   * @param profile
   * @param map
   * @return String viewName
   */
  @RequestMapping(path = "/view", method = RequestMethod.POST)
  public String view(@ModelAttribute("Profile") Profile profile, Model map) {

    Profile resultProfile = null;
    try {
      resultProfile = profileService.getProfile(profile.getId());
    } catch (JsonParseException e) {
      map.addAttribute("error", "A technical error occurred.");
    } catch (JsonMappingException e) {
      map.addAttribute("error", "A technical error occurred.");
    } catch (IOException e) {
      map.addAttribute("error", "A technical error occurred.");
    }
    map.addAttribute("profile", resultProfile);

    return "viewprofile";
  }
}
