package com.junbugtong.controller;

import java.io.IOException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.junbugtong.model.Profile;
import com.junbugtong.service.ProfileService;

/**
 * @author Jun
 *
 */
@RestController
public class ProfileRestController {

  @Autowired
  ProfileService profileService;

  /**
   * Method handler for ajax call of the data table
   * 
   * @param map
   * 
   * @return List<Profile> list of profile from service
   */
  @RequestMapping(value = "/profiles", method = RequestMethod.GET)
  public List<Profile> getAllProfile(Model map) {
    List<Profile> listProfile = null;
    try {
      listProfile = profileService.getAllProfile();
    } catch (JsonParseException e) {
      map.addAttribute("error", "A technical error occurred.");
    } catch (JsonMappingException e) {
      map.addAttribute("error", "A technical error occurred.");
    } catch (IOException e) {
      map.addAttribute("error", "A technical error occurred.");
    }
    return listProfile;
  }
}
