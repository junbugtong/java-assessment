<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Home</title>
<link href="webjars/bootstrap/3.3.7/css/bootstrap.min.css"
	rel="stylesheet" />
<link rel="stylesheet"
	href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css">

<style type="text/css">

.dataTables_length {
    visibility: hidden;
}

.dataTables_wrapper .dataTables_filter {
	position: absolute;
}

label {
	margin-bottom: 20px;
}

#form-profile {
	margin-top: 50px;
}

#profile {
	margin-top: 10px;
}
#profile td {
	border-left: 1.5px solid black;
	border-right: 1.5px solid black;
}
#profile th {
	border-left: 1.5px solid black;
	border-right: 1.5px solid black;
	border-top: 1.5px solid black;
}

</style>

</head>
<body>

	<div class="container">
		<form id="form-profile" modelAttribute="Profile"
			action="/profiles-application/view/" method="post">


			<table id="profile" class="display">

				<!-- Header Table -->
				<thead>
					<tr>
						<th>Name</th>
						<th>Age</th>
						<th>Active</th>
						<th>Blocked</th>
					</tr>
				</thead>
				<!-- Body Table -->
				<tbody>
					<tr>
						<th>Name</th>
						<th>Age</th>
						<th>Active</th>
						<th>Blocked</th>
					</tr>
				</tbody>
			</table>
			<input type="text" name="id" class="invisible" />
		</form>
	</div>

	<script src="webjars/jquery/3.2.1/jquery.min.js"></script>
	<script src="webjars/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<script
		src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
	<script>
		$(document).ready(function() {
			var table = $('#profile').DataTable({
				"sAjaxSource" : "/profiles-application/profiles",
				"sAjaxDataProp" : "",
				"order" : [ [ 0, "asc" ] ],
				"fnCreatedRow" : function(row, data, iDataIndex) {
					$(row).attr('value', data.id);
				},
				"aoColumns" : [ {
					"mData" : getName
				}, {
					"mData" : "age"
				}, {
					"mData" : isActive
				}, {
					"mData" : isBlocked
				} ]
			})

			$('#profile tbody').on('click', 'tr', function() {
				var data = table.row(this).data();
				$('input[name=id]').val(data.id);
				$('#form-profile').submit();

			});

		});

		function getName(data) {
			return data.name.first + " " + data.name.last;
		}

		function isActive(data) {
			if (data.active == "true") {
				return '<input type=\"checkbox\" checked>';
			} else {
				return '<input type=\"checkbox\">';
			}
		}

		function isBlocked(data) {
			if (data.blocked == "true") {
				return '<input type=\"checkbox\" checked>';
			} else {
				return '<input type=\"checkbox\">';
			}
		}
	</script>
</body>
</html>