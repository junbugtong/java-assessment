<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Home</title>
<link
	href="${contextPath}/profiles-application/webjars/bootstrap/3.3.7/css/bootstrap.min.css"
	rel="stylesheet" />

<style type="text/css">
.container img {
	float: left;
}

.container h2 {
	padding: 10px 0px 10px 60px;
}

.container table {
	margin: 25px 0px 0px 7px;
}

.container table tr td {
	padding: 5px 5px 5px 5px;
}

.container table {
	margin-bottom: 10px;
}

.container {
	padding: 20px; border-style : solid;
	margin-top: 50px;
	border-style: solid;
}

.error {
	text-align: center;
	color: red;
}
</style>

</head>
<body>
	<div>
		<h1 class="error">${error}</h1>
	</div>
	<div class="container">

		<div>
			<img src="${profile.picture}" alt="Profile picture" height="50"
				width="50">
			<h2>${profile.name.first} ${profile.name.last}</h2>
		</div>

		<table>
			<tr>
				<td>ID:</td>
				<td>${profile.id}</td>
			</tr>
			<tr>
				<td>Profile:</td>
				<td></td>
			</tr>
			<tr>
				<td></td>
				<td>${profile.profile}</td>
			</tr>
			<tr>
				<td>Email:</td>
				<td>${profile.email}</td>
			</tr>
			<tr>
				<td>Phone:</td>
				<td>${profile.phone}</td>
			</tr>
			<tr>
				<td>Address:</td>
				<td>${profile.address}</td>
			</tr>
			<tr>
				<td>Age:</td>
				<td>${profile.age}</td>
			</tr>
			<tr>
				<td>Balance:</td>
				<td>${profile.balance}</td>
			</tr>
		</table>


	</div>
	<script
		src="${contextPath}/profiles-application/webjars/jquery/3.2.1/jquery.min.js"></script>
	<script
		src="${contextPath}/profiles-application/webjars/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<script>
		$(document).ready(function() {
			if ($('.error').text() != '') {
				$('.container').hide();
			}
		});
	</script>
</body>
</html>